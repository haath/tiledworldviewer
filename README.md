﻿
# TiledWorldViewer

This tool creates a large world out of multiple adjacent Tiled maps, while also watching their directory for changes and reloading the world automatically.
Using this tool one can visualize a multi-map layout in real-time while working on the maps in the Tiled editor.

### Workspace

This tool assumes a directory as the root for developing your maps.

```
.
├── world.yml
├── Maps/
|   ├── east.tmx
|   └── north.tmx
|   ├── south.tmx
├── Tilesets/
```

Where `world.yml` is used to describe the map layout.

```yaml
maps_dir: Maps/

# How many tiles high and tall are the maps
map_width: 32
map_height: 32

maps:

  # Group maps
  overworld:
  - file: north
    position: [0, -1]
  - file: south
    position: [0, 0]
  - file: east
    position: [1, 0]

  dungeon1:
  - ...
```

### Usage

You can either run this tool from the command line, specifying your workspace directory.

```batch
TileWorldViewer.exe "C:/Workspace/Map"
```

Or you can **drag and drop the `world.yml` file into the executable**.