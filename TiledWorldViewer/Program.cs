﻿using System;
using System.IO;

namespace TiledWorldViewer
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
			string dataDir = args.Length > 0 ? args[0] : ".";

			if (dataDir.EndsWith(".yml"))
				dataDir = Path.GetDirectoryName(dataDir);

			if (!Path.IsPathRooted(dataDir))
				dataDir = Path.Combine(Environment.CurrentDirectory, dataDir);

			Console.WriteLine(dataDir);

            using (var game = new Main(dataDir))
                game.Run();
        }
    }
}
