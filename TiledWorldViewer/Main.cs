﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.IO;
using System;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Collections.Generic;

using SharpYaml;
using SharpYaml.Serialization;
using SharpYaml.Serialization.Serializers;

namespace TiledWorldViewer
{

    public class Main : Game
    {
		const float ZOOM_DELTA = 0.1f;

        static GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
		MouseState mouseState;
		string dataDir;

		List<Tilemap> maps;
		bool shouldUpdate;
		Stopwatch updateSw;

		Vector2 pivot;
		float scale = 1f;

		public Main(string dataDir)
        {
			this.dataDir = dataDir;

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
			IsMouseVisible = true;
			Window.AllowUserResizing = true;
        }

        protected override void Initialize()
        {


			/*
			 * Watch data directory for changes
			 */
			FileSystemWatcher watcher = new FileSystemWatcher();
			watcher.Path = dataDir;
			watcher.NotifyFilter = NotifyFilters.LastWrite;
			watcher.Filter = "*.*";
			watcher.EnableRaisingEvents = true;
			watcher.IncludeSubdirectories = true;
			watcher.Changed += (s, args) =>
			{
				shouldUpdate = true;
				updateSw = Stopwatch.StartNew();
			};

			base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

			Reload();
		}

        protected override void UnloadContent()
		{
			Content.Unload();
		}

		protected override void Update(GameTime gameTime)
		{
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();

			if (shouldUpdate && updateSw.ElapsedMilliseconds > 200)
			{
				Console.WriteLine("Reloading...");
				Reload();
				shouldUpdate = false;
			}

			MouseState curState = Mouse.GetState();

			if (mouseState.LeftButton == ButtonState.Pressed)
			{
				pivot.X += (1 / scale) * (curState.X - mouseState.X);
				pivot.Y += (1 / scale) * (curState.Y - mouseState.Y);
			}

			if (curState.ScrollWheelValue > mouseState.ScrollWheelValue)
			{
				scale += ZOOM_DELTA;
			}
			else if (curState.ScrollWheelValue < mouseState.ScrollWheelValue)
			{
				scale -= ZOOM_DELTA;
			}

			if (Keyboard.GetState().IsKeyDown(Keys.Space))
			{
				pivot = Vector2.Zero;
			}

			scale = Clamp(scale, 0.1f, 1.8f);

			mouseState = curState;
			base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.CornflowerBlue);

			Matrix translate = Matrix.CreateTranslation(pivot.X, pivot.Y, 0);
			Matrix zoom = Matrix.CreateScale(scale);

			spriteBatch.Begin(transformMatrix: translate * zoom);
			
			maps.ForEach(m => m.Draw(spriteBatch));

			spriteBatch.End();

			base.Draw(gameTime);
        }

		public void Reload()
		{
			maps = new List<Tilemap>();

			Serializer serializer = new Serializer();
			World world = serializer.Deserialize<World>(File.ReadAllText(Path.Combine(dataDir, "world.yml")));

			foreach (Map map in world.Maps["overworld"])
			{
				if (map != null && map.File != "_")
				{
					string mapPath = Path.Combine(world.MapsDir, map.File + ".tmx");

					Tilemap tilemap = LoadMap(mapPath);

					if (tilemap != null)
					{
						tilemap.Offset = new Vector2(map.X * world.MapWidth, map.Y * world.MapHeight);
						tilemap.LoadContent(graphics.GraphicsDevice, dataDir);

						maps.Add(tilemap);
					}
				}
			}
		}

		public Tilemap LoadMap(string tmxPath)
		{
			tmxPath = Path.Combine(dataDir, tmxPath);

			string tmxFile;

			try
			{
				 tmxFile = File.ReadAllText(tmxPath);
			}
			catch { return null; }
			
			var serializer = new XmlSerializer(typeof(TmxMap));
			var map = (TmxMap)serializer.Deserialize(new StringReader(tmxFile));
			var xmlSerializer = new XmlSerializer(typeof(TmxTileset));

			for (var i = 0; i < map.tilesets.Count; i++)
			{
				var directoryName = Path.GetDirectoryName(tmxPath);
				var tileset = map.tilesets[i];

				if (!string.IsNullOrWhiteSpace(tileset.source))
				{
					var tilesetLocation = tileset.source.Replace('/', Path.DirectorySeparatorChar);
					var filePath = Path.Combine(directoryName, tilesetLocation);

					var normExtTilesetPath = new DirectoryInfo(filePath).FullName;

					using (var file = new StreamReader(filePath))
					{
						map.tilesets[i] = (TmxTileset)xmlSerializer.Deserialize(file);
						map.tilesets[i].fixImagePath(tmxPath, tileset.source);
						map.tilesets[i].firstGid = tileset.firstGid;
						map.tilesets[i].source = Path.Combine(directoryName, tilesetLocation);
					}
				}
				else
				{
					tileset.mapFolder = Path.GetDirectoryName(Path.GetFullPath(tmxPath));
					tileset.image.source = Path.Combine(directoryName, tileset.image.source);
				}
			}

			return new Tilemap(map);
		}

		public static float Clamp(float value, float min, float max)
		{
			return (value < min) ? min : (value > max) ? max : value;
		}
	}
}
