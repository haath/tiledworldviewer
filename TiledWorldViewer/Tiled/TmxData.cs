using System.Collections.Generic;
using System.Xml.Serialization;
using System.Linq;

namespace TiledWorldViewer
{
	public class TmxData
	{
		List<int> tileGids;

		public TmxData()
		{}


		[XmlAttribute( AttributeName = "encoding" )]
		public string encoding;

		[XmlAttribute( AttributeName = "compression" )]
		public string compression;

		[XmlElement( ElementName = "tile" )]
		public List<TmxDataTile> tiles = new List<TmxDataTile>();

		[XmlText]
		public string value;


		public override string ToString()
		{
			return string.Format( "{0} {1}", encoding, compression );
		}

		public List<int> GetTiles()
		{
			if (tileGids == null)
				tileGids = value.Replace("\n", "").Split(',').Select(t => int.Parse(t.Trim())).ToList();
			return tileGids;
		}
	}
}