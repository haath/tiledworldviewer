﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TiledWorldViewer
{
	public class Tilemap
	{
		TmxMap map;

		public Vector2 Offset;

		public Tilemap(TmxMap map)
		{
			this.map = map;
		}

		public void LoadContent(GraphicsDevice graphicsDevice, string dataDir)
		{
			foreach (TmxTileset tileset in map.tilesets)
			{
				string tilesetPath = Path.Combine(dataDir, tileset.image.source);

				using (FileStream fileStream = new FileStream(tilesetPath, FileMode.Open))
				{
					tileset.Texture = Texture2D.FromStream(graphicsDevice, fileStream);
				}
			}
		}

		public void Draw(SpriteBatch batch)
		{
			foreach (TmxTileLayer layer in map.layers.Cast<TmxTileLayer>())
			{
				if (!layer.visible || layer.name == "collision" || layer.name == "walls")
					continue;

				for (int x = 0; x < layer.width; x++)
				{
					for (int y = 0; y < layer.height; y++)
					{
						int tile = layer.data.GetTiles()[x + y * layer.width];

						DrawGid(batch, tile, x, y);
					}
				}

			}
		}

		void DrawGid(SpriteBatch batch, int gid, int posX, int posY)
		{
			foreach (TmxTileset tileset in map.tilesets)
			{
				if (tileset.firstGid <= gid && gid < tileset.firstGid + tileset.tileCount)
				{
					int tileId = gid - tileset.firstGid;
					int x = tileId % tileset.columns;
					int y = tileId / tileset.columns;

					Rectangle subRect = new Rectangle(
						x * tileset.tileWidth,
						y * tileset.tileHeight,
						tileset.tileWidth,
						tileset.tileHeight
					);

					Vector2 position = new Vector2((Offset.X + posX) * tileset.tileWidth, (Offset.Y + posY) * tileset.tileHeight);

					batch.Draw(tileset.Texture, position, subRect, Color.White);

					return;
				}
			}
		}
	}
}
