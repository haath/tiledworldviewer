﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiledWorldViewer
{
	public delegate string ElapsedFormatter(TimeSpan ts);

	public class Coach
	{
		Stopwatch sw;
		StackFrame lastFrame;

		public ElapsedFormatter Formatter = (ts) => ts.TotalMilliseconds + "ms";

		static StackFrame CurrentFrame
		{
			get
			{
				StackFrame frame;
				int skip = 2;

				do
				{
					frame = new StackFrame(skip++, true);
				}
				while (frame.GetMethod().DeclaringType == typeof(Coach));

				return frame;
			}
		}

		private Coach()
		{
			lastFrame = CurrentFrame;
			sw = Stopwatch.StartNew();
		}

		public static Coach StartNew()
		{
			return new Coach();
		}

		public TimeSpan Lap()
		{
			sw.Stop();
			TimeSpan elapsed = sw.Elapsed;

			StackFrame curFrame = CurrentFrame;

			string fileName = Path.GetFileName(curFrame.GetFileName());
			int lineNumber = curFrame.GetFileLineNumber();

			Console.WriteLine("{0}:{1}-{2}\t{3}", fileName, lastFrame.GetFileLineNumber(), lineNumber, Formatter(elapsed));

			lastFrame = curFrame;
			sw.Restart();

			return elapsed;
		}
	}
}
