﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpYaml.Serialization;

namespace TiledWorldViewer
{
	public class World
	{
		[YamlMember("maps_dir")]
		public string MapsDir;

		[YamlMember("map_width")]
		public int MapWidth;

		[YamlMember("map_height")]
		public int MapHeight;

		[YamlMember("maps")]
		public Dictionary<string, List<Map>> Maps;
	}
}
