﻿using SharpYaml.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;

namespace TiledWorldViewer
{
	public class Map
	{
		[YamlMember("file")]
		public string File;

		public int X => position[0];

		public int Y => position[1];

		[YamlMember("position")]
		int[] position;
	}
}
